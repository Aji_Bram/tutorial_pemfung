lengtMS l = sum (map (const 1) l)

-- lengthMS l = sum (map keSatu l)
--              where keSatu _ = 1

-- lengthMS [1,2,3,4,5] = sum (map keSatu [1,2,3,4,5])
--                      = sum ([1,1,1,1,1])
--                      = 1 + 1 + 1 + 1 + 1
--                      = 5

-- map (+1) (map (+1) xs) will iterate a list and add all its element(s) by 1 and repeat it one more time
-- for example map (+1) (map (+1) [1,2,3,4]) will iterate the list and add each element by, thus it will be
-- [2,3,4,5] and the process is repeated so it will produces [3,4,5,6]
-- we can do map f (map g xs) as long as f and g define a function that can operated by element inside function function (such as (+1), abs, reverse etc)
-- otherwise it will produces error


applyTwice :: (a -> a) -> a -> a  
applyTwice f x = f (f x)

iter :: Integral n => n -> (a -> a) -> a -> a
iter 0 f x = x
iter n f x = iter (n-1) f (f x)

-- \n -> iter n succ is an anonymous function (function without name) that  will run the iter function
-- by taking 2 parameters, n and the number we want the succ is. for example (\n -> iter n succ) 4 5
-- will equal to iter 4 f 5 where f 5 is succ 5 (6) and will return 9

sumn :: (Eq t, Floating t, Enum t) => t -> t
sumn 0 = 1
sumn x = foldr (+) 0 (map (**2) [1..x])


mystery xs = foldl (++) [] (map sing xs)
    where sing x = [x]

-- this code will move a list into an empty list
-- first, it will take a list as parameter, then it will iterate each element using map and turn each element of the list become a list contain only its value
-- then it will create a list of list. lastly, it will move each element into new list and append with every element inside list of created by map function

f :: Int -> Bool
f 0 = False
f x = True

id' x = x


-- (id' . f) x will equal to id' (f x) but (id' . f 10) will produce error
-- (f . id') x will equal to f (id' x) or f x itself but (f . id' 10) will produce error
-- (id' f x) will equal to id' (f x)
-- id' will behave as its general type a -> a in (id' .f ) x and (id' f x)

-- 'flip' function whose gonna flip the argument, for example
-- the argument is div 3 100 when we use flip like this flip div 3 100
-- it does change to div 100 3

-- List Comprehension to Higher-Order Functions
-- [ x+1 | x <- xs ]
plusOne :: Num a => [a] -> [a]
plusOne xs = (map (+1) xs)

--sumxy' xs ys = [ x+y | x <- xs, y <-ys ]
sumxy :: Num a => [a] -> [a] -> [a]
sumxy xs ys = foldr (++) [] (map (\x -> map (\y -> x + y) ys) xs)

-- [ x+2 | x <- xs, x > 3 ]
plusTwo :: (Num a, Ord a) => [a] -> [a]
plusTwo xs = (map (+2) $ (filter (>3) xs))

-- [ x+3 | (x,_) <- xys ]
takeX xys = map (+3) $ foldr (++) [] $ (map (\(x,y) -> x) xys)

-- [ x+4 | (x,y) <- xys, x+y < 5 ]
listFour xys = filter (>5) (map (\(x,y) -> x+y) xys)

-- [ x+5 | Just x <- mxs ]
-- Belum menemukan

-- Higher-Order Functions to List Comprehension
-- 1. map (+3) xs
plusThree :: Num a => [a] -> [a]
plusThree xs = [ x+3 | x <- xs ]

-- 2. filter (>7) xs
moreSeven :: (Num a,Ord a) => [a] -> [a]
moreSeven xs = [ x | x <- xs, x > 7 ]

-- 3. concat (map (\x -> map (\y -> (x,y)) ys) xs)
makeList xs ys = [(x,y) | x <- xs, y <- ys]

-- 4. filter (>3) (map (\(x,y) -> x+y) xys)
filterList xys = [x+y | (x,y) <- xys, x+y > 3]

--Primes
primes = sieve [2 ..]
  where sieve (x:xs) = x : (sieve [z | z <- xs, z `mod` x /= 0])

--KPK
kpk x y = [z | z <- [x,x+x..], z `mod` y == 0] !! 0

--FPB
gcd' :: Integer -> Integer -> Integer
gcd' a b
    | b == 0     = abs a
    | otherwise  = gcd' b (a `mod` b)

--maxTiga
maxTiga x y z = max (max x y) z
maxTiga' x y z = max' (max' x y) z
    where max' x y = if x > y then x else y
maxTiga'' x y z =
    let max'' x y = if x > y then x else y
    in max'' (max'' x y) z


zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]  
zipWith' _ [] _ = []  
zipWith' _ _ [] = []  
zipWith' f (x:xs) (y:ys) = f x y : zipWith' f xs ys 

quicksort :: (Ord a) => [a] -> [a]    
quicksort [] = []    
quicksort (x:xs) =     
    let smallerSorted = quicksort (filter (<=x) xs)  
        biggerSorted = quicksort (filter (>x) xs)   
    in  smallerSorted ++ [x] ++ biggerSorted 

quicksort' [] = []
quicksort' (x:xs) = smallerSorted ++ [x] ++ biggerSorted
    where smallerSorted = quicksort (filter (<=x) xs)  
          biggerSorted = quicksort (filter (>x) xs)

quickSort'' [] = []
quickSort'' (x:xs) = quickSort'' [y | y <- xs, y <= x] ++ [x] ++ quickSort'' [y | y <- xs, y > x]

--From Biggest
bubbleSort :: Ord a => [a] -> [a]
bubbleSort ts = foldl swapTill [] ts

swapTill [] x = [x]
swapTill (y:xs) x  = max x y : swapTill xs (min x y)

--From Smallest
bubbleSort' :: Ord a => [a] -> [a]
bubbleSort' = foldr swapTill' []

swapTill' x [] = [x]
swapTill' x (y:xs) = min x y : swapTill' (max x y) xs

--Pytagoras
pythagoras' = [ (x,y,z) | z <- [5 ..], y <- [4 ..z], x <- [3 .. y], x^2 + y^2 == z^2]

--Generating List
-- listOfLength :: Integer -> Gen a -> Gen [a]

--Soal Latihan UTS
--1. misal map (+1) list
mapSucc' [] = []
mapSucc' (x:xs) = [x+3] ++ mapSucc' xs

--Factorial
factorial x = foldr (*) 1 [1..x]
factorial' x = product [1..x]
permutasi x k = (factorial x) / (factorial (x-k))

