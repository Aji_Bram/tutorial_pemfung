f x = x
iter n f x
    | n==0 = x
    | otherwise = f (iter (n-1) f x)

succ x = x + 1
iter n succ x
    | n==0 = x
    | otherwise = succ (iter (n-1) succ x)

xs = [1..10]
sing x = x*x
foldr (+) 0 (map sing xs)